/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controle;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/*import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;*/


public class ConexaoBD {
    
    //public Statement stm; //responsável por buscar em BD
    //public ResultSet ra;  //armazena o resultado dessa pesquisa
    private static final String driver = "com.mysql.jdbc.Driver"; //identifica o serviço no BD
    private static final String caminho = "jdbc:mysql://localhost:3306/bibliotec"; //diz qual o caminho do BD
    private static final String usuario = "root";
    private static final String senha = "serra";
    public Connection con; //realiza a conexão com o BD
    
    public static Connection getConnection(){
       
       
        try {
            Class.forName(driver);
            
            return DriverManager.getConnection(caminho, usuario, senha);
            
        } catch (ClassNotFoundException | SQLException ex) {
            throw new RuntimeException("erro na conexão", ex);
        }
    
    }
    
    public static void closeConnection(Connection con){       
        if(con != null){           
            try {
                con.close();
            } catch (SQLException ex) {
                System.err.println("Erro: "+ex);
            }            
        }  
    }
    
    public static void closeConnection(Connection con, PreparedStatement stmt, ResultSet rs){
       
        if(rs != null){          
            try {
                rs.close();
            } catch (SQLException ex) {
                System.err.println("Erro: "+ex);            
            } 
          
        } 
        closeConnection(con);
    }
    
   /* public void conexao(){
        
        try {
            System.setProperty("jdbc.Drivers", driver);
            con=DriverManager.getConnection(caminho, usuario, senha);
            JOptionPane.showMessageDialog(null, "Conexão efetuada com sucesso!");
         } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Erro ao se conectar com o banco de dados!\n"+ex);
        }
    }*/

    public void conexao() {
        
    }

    public void desconecta() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    

   
}
